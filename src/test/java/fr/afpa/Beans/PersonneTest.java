package fr.afpa.Beans;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor

@Entity
@NamedQuery(name="findLog", query="select p from Personne p where p.nom = : value")

public class PersonneTest {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	//@SequenceGenerator(name = "Personne_generator", sequenceName = "Personne_seq")
	int idPersonne;

	String nom;
	String password;
	String prenom;
	String nomLibrairie;
	int numVoie;
	String nomRue;
	int codePostal;
	String ville;
	String mail;
	String telephone;
	
	@OneToMany(mappedBy = "personne")	
	private List <Annonces> annonces;

}
