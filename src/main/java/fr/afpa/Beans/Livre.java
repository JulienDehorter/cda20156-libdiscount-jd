package fr.afpa.Beans;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

/*
 * Classe créant les livres
 */
@Entity
public class Livre {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "Livre_generator")
	@SequenceGenerator(name = "Livre_generator", sequenceName = "Livre_seq")
	
	String titre;
	String niveau;
	Long isbn;
	String date;
	String editeur;
	float prix;
	int quantite;	
	@ManyToOne(cascade = { CascadeType.ALL })
	private Annonces annonces;
	
	
	

}
