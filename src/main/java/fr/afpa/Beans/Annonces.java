package fr.afpa.Beans;

import java.util.List;


import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

/*
 * Classe créant les annonces. Une annonce possède une liste de livres
 */
@Entity
public class Annonces {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "Annonces_generator")
	@SequenceGenerator(name = "Annonces_generator", sequenceName = "Annonces_seq")

	int remise;
	int prixTotal;
	@OneToMany(mappedBy = "annonces")	
	private List <Livre> livre;
	
	@ManyToOne(cascade = { CascadeType.ALL })
	private Personne personne;
}
