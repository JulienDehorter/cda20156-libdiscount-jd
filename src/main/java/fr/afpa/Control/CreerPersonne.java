package fr.afpa.Control;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;

import fr.afpa.Beans.Personne;
import fr.afpa.DAO.GestionPersonne;
import fr.afpa.DAO.HibernateUtils;

/**
 * Servlet controlant la saisie des informations pour créer une personne
 */
public class CreerPersonne extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public CreerPersonne() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		String nom = request.getParameter("nom");
		String password = request.getParameter("password");
		
		GestionPersonne gp = new GestionPersonne();
		if(!gp.CompteExiste(nom, password)){		

		Personne per = new Personne();
		per.setNom(request.getParameter("nom"));
		per.setPassword(request.getParameter("password"));
		per.setPrenom(request.getParameter("prenom"));
		per.setNomLibrairie(request.getParameter("librairie"));
		per.setNumVoie(Integer.parseInt(request.getParameter("numVoie")));
		per.setNomRue(request.getParameter("nomRue"));
		per.setCodePostal(Integer.parseInt(request.getParameter("codePostal")));
		per.setVille(request.getParameter("ville"));
		per.setMail(request.getParameter("mail"));
		per.setTelephone(request.getParameter("telephone"));
		
		request.setAttribute("maPersonne", per);		
		request.getRequestDispatcher("/WEB-INF/CompteCree.jsp").forward(request, response);}
	}

	

}
