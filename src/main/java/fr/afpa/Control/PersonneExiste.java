package fr.afpa.Control;

import java.io.IOException;
import java.util.ArrayList;

import javax.persistence.Query;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.hibernate.Session;
import org.hibernate.Transaction;

import fr.afpa.Beans.Personne;
import fr.afpa.DAO.GestionPersonne;
import fr.afpa.DAO.HibernateUtils;

/**
 * Servlet controlant si les informations de login sont correctes. Si oui, selon les informations saisies, elle renvoie vers une page d'administration (LogAdminSuccessful.jsp), ou une page utilisateur (LogSuccessfull.jsp)
 */
public class PersonneExiste extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public PersonneExiste() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 * 
	 */

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String nom = (String) request.getParameter("nom");
		String password = (String) request.getParameter("password");
		String path = "/WEB-INF/LogSuccessfull.jsp";

		GestionPersonne gp = new GestionPersonne();

		Personne per = gp.recupPersonneParNom(nom);

		if (per.getNom().toString().equals(nom) && per.getPassword().toString().equals(password)) {
			if (per.getIdPersonne() == (1)) {
				path = "WEB-INF/LogAdminSuccessful.jsp";
			}
			request.setAttribute("maPersonne", per);
			request.getRequestDispatcher(path).forward(request, response);

		} else {
			System.out.println("personne non trouvée");
		}
		
		
	}

}
