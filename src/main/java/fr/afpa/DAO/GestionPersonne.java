package fr.afpa.DAO;

import java.util.ArrayList;

import javax.persistence.Query;

import org.hibernate.Session;
import org.hibernate.Transaction;

import fr.afpa.Beans.Personne;

/*
 * Classe permettant d'interagir avec la BDD, au sujet des personnes
 */
public class GestionPersonne {
	
	private Session s;
	
	/*
	 * Enregistre une nouvelle personne
	 */
	public void EnregistrerPersonne (Personne per) {
		
		HibernateUtils h = new HibernateUtils();
		Session s = HibernateUtils.getSession();

		Transaction tx = s.beginTransaction();

		s.persist(per);
	
		tx.commit();
		
		s.close();
		
	}
	
	/*
	 * Liste toutes les personnes existantes
	 */
	public ArrayList<Personne> listerPersonnes() {
		
		
		 s = HibernateUtils.getSession();

	Query q = s.createQuery("from Personne");
	
	 ArrayList<Personne> listePersonnes = ( ArrayList<Personne>) q.getResultList();		
		
		s.close();
		
		return listePersonnes;	
		
	}
	
	/*
	 * Retourne une personne, en fonction de son nom
	 */
	public Personne recupPersonneParNom(String nom) {
		
		Session s = HibernateUtils.getSession();
		
		Query q = s.getNamedQuery("findLog");
		
		q.setParameter("value", nom);
		
		Personne personne = (Personne) q.getSingleResult();
		
		s.close();
		
		return personne;
		
	}
	
	/*
	 * Vérifie si un compte existe, en fonction du nom et du password
	 */
	public boolean CompteExiste(String nom, String password) {		
		
		Personne per = recupPersonneParNom(nom);
		if (per.getNom().toString().equals(nom) && per.getPassword().toString().equals(password)) {
			return true;
		} else {
			return false;
		}
	
	}

}
